## YouTube Blocks

This module provides a collection of custom blocks to pull YouTube
videos in.

 * Visit the module's project page:
   https://drupal.org/project/youtube_block

### Requirements
This module requires the following modules:

* Block (Drupal Core)
* Key (https://www.drupal.org/project/key)

### Composer Dependencies

This module depends upon Composer to install (it's dependencies).

* php-youtube-api - (https://github.com/madcoda/php-youtube-api)

### Composer Install

```bash
composer require drupal/youtube_block
```

### Features

* Provides collection of blocks to display YouTube videos.
* Pull in YouTube videos by channel.
* Pull in YouTube videos by playlist.
* Pull in YouTube video by video id.

### Installation

Install as you would normally install a contributed Drupal module. See:
  https://www.drupal.org/documentation/install/modules-themes/modules-8
  for further information.

### Configuration

After you have installed the module,  navigate to the configuration
page at **/admin/config/services/youtube-block**.  Once there, you need to
configure your YouTube API key. Using the Key module, you will use the
"Manage Keys" page to set up new key. Once setup, you can then link it
to this module.

**Add a YouTube Block to a block region**

* Home > Administration > Structure > Block layout
* Click 'Place block' of the region you want to place a YouTube Block in.
* Search for 'YouTube Blocks' in the listed blocks and click 'Place block'.
* Configure block and required settings.
* Adjust "Visibility" settings.
* Save the block and view it.

### Maintainers
Current maintainer:
* George Anderson (geoanders) https://www.drupal.org/u/geoanders

Past maintainers:
* David Thompson (dbt102) https://www.drupal.org/user/338300
