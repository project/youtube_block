<?php

namespace Drupal\youtube_block\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure youtube_block settings.
 */
class YoutubeBlockForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'youtube_block_config_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['youtube_block.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('youtube_block.settings');

    $form['api_key_name'] = [
      '#type' => 'key_select',
      '#title' => $this->t('Youtube API Key'),
      '#description' => $this->t('Google API key used to access Youtube services.'),
      '#empty_option' => $this->t('- Select Key -'),
      '#default_value' => $config->get('api_key_name'),
      '#key_filters' => ['type' => 'authentication'],
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('youtube_block.settings')
      ->set('api_key_name', $form_state->getValue('api_key_name'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
