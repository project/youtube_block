<?php

namespace Drupal\youtube_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\youtube_block\YoutubeBlockService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a YouTube by playlist block.
 *
 * @Block(
 *   id = "youtube_by_playlist_block",
 *   admin_label = @Translation("YouTube By Playlist Id"),
 *   category = @Translation("YouTube Blocks")
 * )
 */
class YoutubeByPlaylistBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The YouTube block service.
   *
   * @var \Drupal\youtube_block\YoutubeBlockService
   */
  protected $youtubeBlockService;

  /**
   * Constructs a YoutubeByPlaylistBlock object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\youtube_block\YoutubeBlockService $youtube_block_service
   *   The youtube block service.
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, YoutubeBlockService $youtube_block_service) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->youtubeBlockService = $youtube_block_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('youtube_block')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function settings() {
    return [
      'total_items' => 25,
      'playlist_id' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['total_items'] = [
      '#type' => 'number',
      '#title' => $this->t('Total items to show'),
      '#description' => $this->t('Total number of videos/items to show on block.'),
      '#min' => 0,
      '#max' => 50,
      '#default_value' => $this->configuration['total_items'] ?? 25,
    ];

    $form['playlist_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('YouTube Playlist Id'),
      '#description' => $this->t('Pull videos down by playlist id.'),
      '#default_value' => $this->configuration['playlist_id'] ?? '',
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockValidate($form, FormStateInterface $form_state) {
    if (!$this->youtubeBlockService->getPlaylistById($form_state->getValue('playlist_id'))) {
      $form_state->setErrorByName('playlist_id', $this->t('Invalid playlist id provided. Please provide a valid playlist id.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    if ($form_state->hasAnyErrors()) {
      return;
    }
    else {
      $this->configuration['playlist_id'] = $form_state->getValue('playlist_id');
      $this->configuration['total_items'] = $form_state->getValue('total_items');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [
      '#theme' => 'youtube_block_playlist',
      '#title' => NULL,
      '#description' => NULL,
      '#items' => [],
    ];

    $playlist = NULL;
    $playlist_id = $this->configuration['playlist_id'];
    $total_items = $this->configuration['total_items'];

    if (!empty($playlist_id)) {
      $playlist = $this->youtubeBlockService->getPlaylistById($playlist_id);
    }

    if (empty($playlist_id)) {
      return [];
    }

    $build['#title'] = $playlist->snippet->title;
    $build['#description'] = $playlist->snippet->description;

    $items = $this->youtubeBlockService->getPlaylistItemsByPlaylistId($playlist->id);
    $item_count = 0;
    foreach ($items as $item) {
      if ($item_count == $total_items) {
        break;
      }

      $image = NULL;
      if (isset($item->snippet->thumbnails->high)) {
        $image = $item->snippet->thumbnails->high;
      }
      if (!$image && isset($item->snippet->thumbnails->standard)) {
        $image = $item->snippet->thumbnails->standard;
      }

      $build['#items'][$item->id] = [
        '#theme' => 'youtube_block_video',
        '#vid' => $item->contentDetails->videoId,
        '#title' => $item->snippet->title,
        '#image' => $image,
      ];

      $item_count++;
    }

    $build['#attached']['library'][] = 'youtube_block/youtube_block';

    return $build;
  }

}
