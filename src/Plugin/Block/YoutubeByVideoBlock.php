<?php

namespace Drupal\youtube_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\youtube_block\YoutubeBlockService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a YouTube by video block.
 *
 * @Block(
 *   id = "youtube_by_video_block",
 *   admin_label = @Translation("YouTube By Video Id"),
 *   category = @Translation("YouTube Blocks")
 * )
 */
class YoutubeByVideoBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The YouTube block service.
   *
   * @var \Drupal\youtube_block\YoutubeBlockService
   */
  protected $youtubeBlockService;

  /**
   * Constructs a YoutubeByChannelBlock object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\youtube_block\YoutubeBlockService $youtube_block_service
   *   The youtube block service.
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, YoutubeBlockService $youtube_block_service) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->youtubeBlockService = $youtube_block_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('youtube_block')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function settings() {
    return [
      'video_id' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['video_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('YouTube Video Id'),
      '#description' => $this->t('Pull video down by video id.'),
      '#default_value' => $this->configuration['video_id'] ?? '',
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockValidate($form, FormStateInterface $form_state) {
    if (!$this->youtubeBlockService->getVideoInfo($form_state->getValue('video_id'))) {
      $form_state->setErrorByName('video_id', $this->t('Invalid video id provided. Please provide a valid video id.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    if ($form_state->hasAnyErrors()) {
      return;
    }
    else {
      $this->configuration['video_id'] = $form_state->getValue('video_id');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [
      '#theme' => 'youtube_block_single_video',
    ];

    $video = NULL;
    $video_id = $this->configuration['video_id'];

    if (!empty($video_id)) {
      $video = $this->youtubeBlockService->getVideoInfo($video_id);
    }

    if (empty($video)) {
      return [];
    }

    $build['#vid'] = $video_id;
    $build['#title'] = $video->snippet->title;
    $build['#description'] = $video->snippet->description;

    if (isset($video->snippet->thumbnails->high)) {
      $build['#image'] = $video->snippet->thumbnails->high;
    }
    if (!$build['#image'] && isset($video->snippet->thumbnails->standard)) {
      $build['#image'] = $video->snippet->thumbnails->standard;
    }

    $build['#attached']['library'][] = 'youtube_block/youtube_block';

    return $build;
  }

}
