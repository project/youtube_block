<?php

namespace Drupal\youtube_block;

use Drupal\Core\Config\ConfigFactory;
use Drupal\key\KeyRepository;
use Madcoda\Youtube\Youtube;

/**
 * Class Youtube Block Service.
 */
class YoutubeBlockService {

  /**
   * The config factory object.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * The key repository object.
   *
   * @var \Drupal\key\KeyRepository
   */
  protected $keyRepository;

  /**
   * The YouTube client.
   *
   * @var \Madcoda\Youtube\Youtube
   */
  private $client;

  /**
   * The constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   The config factory.
   * @param \Drupal\key\KeyRepository $key_repository
   *   The key repository.
   */
  public function __construct(ConfigFactory $config_factory, KeyRepository $key_repository) {
    $this->configFactory = $config_factory;
    $this->keyRepository = $key_repository;
  }

  /**
   * Get Api Key.
   *
   * @return string|null
   *   The api key.
   */
  public function getApiKey(): ?string {
    $apiKeyName = $this->configFactory->get('youtube_block.settings')->get('api_key_name');
    $apiKey = NULL;
    if (!empty($apiKeyName)) {
      $apiKey = $this->keyRepository->getKey($apiKeyName)->getKeyValue();
    }
    return $apiKey;
  }

  /**
   * Get API client.
   *
   * @return \Madcoda\Youtube\Youtube
   *   The client.
   *
   * @throws \Exception
   */
  public function getClient(): Youtube {
    if (!$this->client) {
      $this->client = new Youtube(['key' => $this->getApiKey()]);
    }
    return $this->client;
  }

  /**
   * Get video info.
   *
   * @param string $vid
   *   The video id.
   *
   * @return false|\StdClass
   *   The video or false if not found.
   *
   * @throws \Exception
   */
  public function getVideoInfo($vid) {
    return $this->getClient()->getVideoInfo($vid);
  }

  /**
   * Get channel by username.
   *
   * @param string $username
   *   The username.
   *
   * @return false|\StdClass
   *   The channel or false if not found.
   *
   * @throws \Exception
   */
  public function getChannelByName($username) {
    return $this->getClient()->getChannelByName($username);
  }

  /**
   * Get channel by id.
   *
   * @param string $id
   *   The channel id.
   *
   * @return false|\StdClass
   *   The channel or false if not found.
   *
   * @throws \Exception
   */
  public function getChannelById($id) {
    return $this->getClient()->getChannelById($id);
  }

  /**
   * Get playlists by channel id.
   *
   * @param string $id
   *   The channel id.
   *
   * @return array|false
   *   The playlists or false if not found.
   *
   * @throws \Exception
   */
  public function getPlaylistsByChannelId($id) {
    return $this->getClient()->getPlaylistsByChannelId($id);
  }

  /**
   * Get playlist by id.
   *
   * @param string $id
   *   The playlist id.
   *
   * @return false|\StdClass
   *   The playlist or false if not found.
   *
   * @throws \Exception
   */
  public function getPlaylistById($id) {
    return $this->getClient()->getPlaylistById($id);
  }

  /**
   * Get playlist items by playlist id.
   *
   * @param string $id
   *   The playlist id.
   * @param int $maxResults
   *   The max results.
   *
   * @return array|false
   *   The playlist items or false if not found.
   *
   * @throws \Exception
   */
  public function getPlaylistItemsByPlaylistId($id, $maxResults = 50) {
    return $this->getClient()->getPlaylistItemsByPlaylistId($id, $maxResults);
  }

}
